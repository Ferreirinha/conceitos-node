const express = require('express');

const app = express();

app.use(express.json());

/**
 * GET - buscar uma Informação 
 * POST - inserir uma Informação 
 * PUT - alterar uma Informação 
 * PATCH - alterar uma Informação especifica
 * DELETE - Deletar uma Informação 
*/

/**
 * Tipos de parametros
 * Route Params => identificar um recurso especifico
 * Query Params => Paginação / Filtro 
 * Body Params => Objetos passados para Inserção ou Alteração 
 */


app.get("/courses", (request, response) => {
    //exemplo de query params 
    const query = request.query;
    console.log(query);
    return response.json([
        "Curso 1", "Curso 2", "Curso 3"
    ]);
});

app.post("/courses", (request, response)=>{
    const body = request.body;
    console.log(body);
    return response.json(["Curso 1", "Curso 2", "Curso 3", "Curso 4"])
});

app.put("/courses/:id", (request, response)=>{
    //exemplo de route params /:id
    const { id } = request.params;
    console.log(id);
    return response.json(["Curso 6", "Curso 2", "Curso 3", "Curso 4"])
});

app.patch("/courses/:id", (request, response)=>{
    return response.json(["Curso 6", "Curso 7", "Curso 3", "Curso 4"])
});

app.delete("/courses/:id", (request, response)=>{
    return response.json(["Curso 6"])
});

app.listen(1899); 

